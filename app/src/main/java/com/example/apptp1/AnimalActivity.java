package com.example.apptp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class AnimalActivity extends AppCompatActivity {

    private TextView nomAnimalLayout;
    private  EditText conservationLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        //On reprend le nom de l'animal sur lequel on a clique => descNomAnimal

        Intent intent = getIntent();
        String descNomAnimal = ((Intent) intent).getStringExtra("Animal");

        //Changement titre en fonction de l'animal
        nomAnimalLayout = findViewById(R.id.descNomAnimal);
        nomAnimalLayout.setText(descNomAnimal);

        //On reprend les infos de l'animal choisit => animalSelectionne
        AnimalList animalList = new AnimalList();
        final Animal animalSelectionne = animalList.getAnimal(descNomAnimal);


        //------Changement image en fonction de l'animal------
        String nomImage = animalSelectionne.getImgFile();
        Context c =getApplicationContext();
        int imageID= c.getResources().getIdentifier("drawable/"+nomImage, null, c.getPackageName());
        ((ImageView)findViewById(R.id.descImage)).setImageResource(imageID);

        ////------Changement esperance en fonction de l'animal------
        String esperance=animalSelectionne.getStrHightestLifespan();
        ((TextView)findViewById(R.id.descEsperance)).setText(esperance);

        //------Changement gestation en fonction de l'animal------
        String gestation=animalSelectionne.getStrGestationPeriod();
        ((TextView)findViewById(R.id.descGestation)).setText(gestation);

        //------Changement Poids de naissance en fonction de l'animal------
        String pdsNaissance=animalSelectionne.getStrBirthWeight();
        ((TextView)findViewById(R.id.descPdsNaissance)).setText(pdsNaissance);

        //------Changement poids d'adulte en fonction de l'animal------
        String pdsAdulte=animalSelectionne.getStrAdultWeight();
        ((TextView)findViewById(R.id.descPdsAdulte)).setText(pdsAdulte);

        //------Changement etat de conservation en fonction de l'animal------
        String conservation=animalSelectionne.getConservationStatus();
        conservationLayout=(EditText)findViewById(R.id.descConservation);
       conservationLayout.setText(conservation);

        //Pour specifier une action quand le bouton est appuye => click listener sur l'objet bouton
        final Button boutonSauvegarde= findViewById(R.id.btnSauvegarde);
        boutonSauvegarde.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animalSelectionne.setConservationStatus(String.valueOf(conservationLayout.getText()));
            }
        });
    }
}
