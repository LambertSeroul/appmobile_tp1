package com.example.apptp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    private ListView listeAnimaux;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        listeAnimaux=findViewById(R.id.ListeAnimaux);
        AnimalList animal = new AnimalList();
        final ArrayAdapter<String> adapter= new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, animal.getNameArray());
        listeAnimaux.setAdapter(adapter);

        listeAnimaux.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final String objet = (String) parent.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.this, AnimalActivity.class);
                intent.putExtra("Animal", objet);
                MainActivity.this.startActivity(intent);
            }
        });

    }
}

